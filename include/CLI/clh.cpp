#include "clh.h"
#include <cstdlib>
#include <functional>
#include <iostream>
#include <ostream>
#include <string>


Menu::Menu(unsigned size){
	menu = new menuItem[size];
	menuSize = size;
	showNumbers = true;
}

Menu::~Menu(){
	delete[] menu;
}

void Menu::enableDefItems(bool exitItem, bool clearItem, bool showMenuItem){
	defItems = {exitItem, clearItem, showMenuItem};
}

void Menu::setShowNumbers(bool sn){
	showNumbers = sn;
}

void Menu::addItem(std::string name, std::function<void(int)> callback, std::string cmd, int pos){
	pos = (pos <= -1 ? position : pos);
	if(pos > menuSize){
		std::cout << "Array out of bounds" << std::endl;
		exit(1);
	}

	auto defCall = [](int curPos){
		std::cout << "Entred item: " << curPos << std::endl;
	};
	
	menu[pos] = {name, (callback == nullptr ? defCall : callback)};

	position++;

	//menuItem *pMenu = menu;//new menuItem[position];
	//delete []menu;
	//menu = new menuItem[position];
	/* for(int i{0}; i<position; i++) */
	/* 	menu[i] = pMenu[i]; */
}

void Menu::show(){
	//std::system("clear");

	std::string menuStr;
	for(int i{0}; i<position; i++)
		menuStr += (showNumbers ? std::to_string(i+1) + ") " : "") + menu[i].name + "\n";

	std::cout << menuStr;

	//std::cout << std::endl;
}

void Menu::run_v2(){
	/*int curPos = 0;

	std::system("clear");

	show();

	std::cout << getch() << std::endl;*/

	std::cout << "!!!" << std::endl << "Sory, but \"run_v2\" fuction doesnt work now. To the next will be used default \"run\" function" << std::endl;
	run();
}

void Menu::run(bool loop){
	static bool isRun = false;
	if(!isRun){
		show();
		isRun = true;
	}
	//if(!loop) show();

	std::string cmd;
	
	std::cout << "> ";
	std::getline(std::cin, cmd);

	for(int i{0}; i<menuSize; i++){
		if((std::to_string(i+1) == cmd) || (cmd == menu[i].cmd))
			menu[i].callback(i);
	}	

	if(loop)
		run(loop);
}
