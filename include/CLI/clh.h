#include <functional>
#include <string>

class Menu {
	public:
		Menu(unsigned=10);
		~Menu();
		void addItem(std::string, std::function<void(int)> =nullptr, std::string="qwerty", int=-1);
		void show();
		void run(bool=false);
		void run_v2();
		void enableDefItems(bool=false, bool=false, bool=false); // exit, clear, show menu
		void setShowNumbers(bool);
	private:
		struct menuItem {
			std::string name;
			std::function<void(int)> callback;
			std::string cmd;
		};

		menuItem *menu;
		int position;
		unsigned menuSize;
		bool showNumbers;

		struct {
			bool exitItem;
			bool clearItem;
			bool showMenuItem;
		} defItems;
};
