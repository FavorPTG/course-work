#include "../sqlite/sqlite3.h"
#include <string>
#include <iostream>
#include "./dbw.h"

DBWC::DBWC(sqlite3 *base){
	db = base;
}

int DBWC::createPlaneTable(){
    const string createTableQuery =
            "CREATE TABLE IF NOT EXISTS planes ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "standend TEXT NOT NULL,"
            "stime TEXT NOT NULL,"
            "etime TEXT NOT NULL,"
            "rnumb INTEGER NOT NULL,"
            "nseats INTEGER NOT NULL,"
            "seatprice INTEGER NOT NULL"
            ");";
    int rc = sqlite3_exec(db, createTableQuery.c_str(), nullptr, nullptr, nullptr);
    if(rc != SQLITE_OK){
        cout << "Error from open database - tickets" << endl;
        return rc;
    }
    return SQLITE_OK;
}

int DBWC::createTicketTable(){
    const string createBaseTableQery =
        "CREATE TABLE IF NOT EXISTS ticket ("
        "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "rnumb INTEGER NOT NULL,"
        "seatnumber INTEGER NOT NULL,"
        "name TEXT NUT NULL);";
    int rc = sqlite3_exec(db, createBaseTableQery.c_str(), nullptr, nullptr, nullptr);
    if(rc != SQLITE_OK){
        cout << "Error from creation table - planes" << endl;
        exit(rc);
        return rc;
    }
    return SQLITE_OK;
}

int DBWC::joinTables(){
    createPlaneTable();
    createTicketTable();
    return 0;
}

int DBWC::findInTable(string tableName, string colName, string fData){
	sqlite3_stmt* stmt;
	const string sql = "SELECT COUNT(" + colName + ") FROM " + tableName + " WHERE " + colName + " = " + fData + ";";
	int result = 0;
	if (sqlite3_prepare(db, sql.c_str(), -1, &stmt, nullptr) == SQLITE_OK) {
        	if (sqlite3_step(stmt) == SQLITE_ROW) {
                	result = sqlite3_column_int(stmt, 0);
            	}
            	sqlite3_finalize(stmt);
        }
	
	return result;
}


int DBWC::getTableSize(bool isPlaneTable){
	std::string query = "SELECT COUNT(*) FROM " + string(isPlaneTable ? "planes" : "tickets") + ";";
	sqlite3_stmt *stmt;
	int res = 0;
	int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);

	if(rc != SQLITE_OK)
		return rc;

	if((rc = sqlite3_step(stmt)) == SQLITE_ROW)
		res = sqlite3_column_int(stmt, 0);

	sqlite3_finalize(stmt);
	return res;
}

DBWC::Plane* DBWC::getDataFromPlaneTable(){
	const unsigned sze = getTableSize();
	DBWC::Plane *pln = new Plane[sze];

	std::string query = "SELECT * FROM planes;";
	sqlite3_stmt *stmt;
	int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);
		
	if(rc != SQLITE_OK)
		return new Plane {0, "Error:" + string(sqlite3_errmsg(db))};
	
	int pos = 0;
	while((rc = sqlite3_step(stmt)) == SQLITE_ROW){
		pln[pos].id =        sqlite3_column_int (stmt, 0);
		pln[pos].sAndE = (char*) sqlite3_column_text(stmt, 1);
		pln[pos].sTime = (char*) sqlite3_column_text(stmt, 2);
		pln[pos].eTime = (char*) sqlite3_column_text(stmt, 3);
		pln[pos].rNumber =   sqlite3_column_int (stmt, 4);
		pln[pos].nSeats =    sqlite3_column_int (stmt, 5);
		pln[pos].seatPrice = sqlite3_column_int (stmt, 6);
		pos++;
	}
   		
	if (rc != SQLITE_DONE) {
		std::cout << "Error from open database" << std::endl;
        	return NULL;
    	}

	sqlite3_finalize(stmt);

	return pln;
}

DBWC::Ticket* DBWC::getDataFromTicketTable(){
	const unsigned sze = getTableSize(false);
	Ticket *tct = new Ticket[sze];

	std::string query = "SELECT * FROM tickets;";
	sqlite3_stmt *stmt;
	int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);

	if(rc != SQLITE_OK)
		return new Ticket {0, 0, 0, "Error: " + string(sqlite3_errmsg(db))};

	int pos = 0;
	while((rc = sqlite3_step(stmt)) == SQLITE_ROW){
		tct[pos].id = sqlite3_column_int(stmt, 0);
		tct[pos].rNumber = sqlite3_column_int(stmt, 1);
		tct[pos].seatNumber = sqlite3_column_int(stmt, 2);
		tct[pos].name = (char*) sqlite3_column_text(stmt, 3);
		pos++;
	}

	if (rc != SQLITE_DONE) {
		std::cout << "Error from open database" << std::endl;
        	return {};
    	}

	sqlite3_finalize(stmt);

	return tct;
}
	

int DBWC::addNewPlaneInTable(DBWC::Plane p){

	string 
		startAndEnd = p.sAndE, 
		sTime = p.sTime, 
		eTime = p.eTime;
    	int 
		rNumber = p.rNumber, 
	    	nSeats = p.nSeats, 
	    	seatPrice = p.seatPrice;


	std::string query = "INSERT INTO planes (standend, stime, etime, rnumb, nseats, seatprice) VALUES ('" +
         	   startAndEnd + "', '" + sTime + "', '" + eTime + "', " + to_string(rNumber) + ", " + to_string(nSeats) + ", " + to_string(seatPrice) + ")";

    	int 
		rc = SQLITE_ERROR,
		q = findInTable("planes", "rnumb", to_string(p.rNumber));
	
	cout << q << " " << query << endl;

	if(q == 0)
		rc = sqlite3_exec(db, query.c_str(), nullptr, nullptr, nullptr);

	cout << q << " " << rc << " " << sqlite3_errmsg(db) << endl;
		
    	if(rc != SQLITE_OK)
        	cout << "Error from adding the new plane" << endl;
    	else 
		cout << "New flight added in the table" << endl;

    	return rc;
}


int DBWC::addNewTicketInTable(DBWC::Ticket t){
    const string instrtQuery = "INSERT INTO ticket (rnumb, seatnumber, name) VALUES (" +
        to_string(t.rNumber) + ", " + to_string(t.seatNumber) + ", '" + t.name + "');";

    int
	rc = SQLITE_ERROR,
	q1 = findInTable("planes", "rnumb", to_string(t.rNumber)),
	q2 = multyFindInTable("ticket", "rnumb", to_string(t.rNumber), "seatnumber", to_string(t.seatNumber)),
	seatC = 0;


    if((q1 != 0) && (q2 == 0)){ 
	    for(int i=1; 1; i++){
		    if(getDataById("planes", "rnumb", i) == t.rNumber){
			    seatC = getDataById("planes", "nseats", i);
			    break;
		    }
	    }
	    if(t.seatNumber <= seatC)
		    rc = sqlite3_exec(db, instrtQuery.c_str(), nullptr, nullptr, nullptr);
    }

    if(rc != SQLITE_OK)
	cout << "Error from adding the new ticket" << endl;
    else
	cout << "Ticket added in table" << endl;

    return rc;
}

int DBWC::multyFindInTable(string tableName, string col1, string f1, string col2, string f2){
	sqlite3_stmt *stmt;
	const string query = "SELECT COUNT(*) FROM " + tableName + " WHERE " + col1 + " = '" + f1 + "' AND " + col2 + " = '" + f2 + "';";
	int result = 0;
	if(sqlite3_prepare(db, query.c_str(), -1, &stmt, nullptr) == SQLITE_OK){
		if(sqlite3_step(stmt) == SQLITE_ROW){
			result = sqlite3_column_int(stmt, 0);
		}
		sqlite3_finalize(stmt);
	}
	return result;
}

int DBWC::getDataById(string tableName, string col, int id){
	sqlite3_stmt* stmt;
    	int res;
	int rc;

    	const string sql = "SELECT " + col + " FROM " + tableName + " WHERE id = ?;";
	rc = sqlite3_prepare(db, sql.c_str(), -1, &stmt, nullptr);

	if (rc != SQLITE_OK) {
		std::cerr << "Ошибка подготовки запроса: " << sqlite3_errmsg(db) << std::endl;
		return 1;
	}

	sqlite3_bind_int(stmt, 1, id);

    	rc = sqlite3_step(stmt);

    	if (rc == SQLITE_ROW) {
		res = sqlite3_column_int(stmt, 0);
	}

	sqlite3_finalize(stmt);

	return res;
}

int DBWC::getDataWithDesc_back(string query, string desc){
	
	sqlite3_stmt *stmt;
	int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);
	if (rc != SQLITE_OK) {
		cout << "error: " << sqlite3_errmsg(db) << endl;
    		return SQLITE_ERROR;
	}
	cout << desc << ":" << endl << "| ";
	while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
		const unsigned char *name = sqlite3_column_text(stmt, 0);
		cout << name << " | ";
	}
	cout << endl;
	
	if (rc != SQLITE_DONE) {
		cout << "error: " << sqlite3_errmsg(db) << endl;
	}
	
	sqlite3_finalize(stmt);

	return SQLITE_OK;
}

int DBWC::getDataWithDesc(string tableName, string colName, string desc){
	const string query = "SELECT " + colName + " FROM " + tableName + ";";
	return getDataWithDesc_back(query, desc);
}

int DBWC::getDataWithDesc(string tableName, string colO, string colS, string fil, string desc){
	const string query = "SELECT " + colO + " FROM " + tableName + " WHERE " + colS + " = " + fil + ";";
	return getDataWithDesc_back(query, desc);
}

int DBWC::deleteFromTable(string tableName, string colName, string data){
	string delete_query = "DELETE FROM " + tableName + " WHERE " + colName + " = " + data + ";";

	char* errMsg;
	int rc = sqlite3_exec(db, delete_query.c_str(), nullptr, nullptr, &errMsg);

	if (rc != SQLITE_OK) {
		cerr << "SQL error: " << errMsg << endl;
        	sqlite3_free(errMsg);
    	} else
        	cout << "Record with " << colName << " " << data << " deleted successfully." << endl;

	return rc;
}

int DBWC::deleteFromTable(string tableName, string id){
	return deleteFromTable(tableName, "id", id);
}

int DBWC::deleteTable(string tableName){
    const string deleteTableQuery = "DROP TABLE IF EXISTS " + tableName;
    int rc = sqlite3_exec(db, deleteTableQuery.c_str(), nullptr, nullptr, nullptr);
    return rc;
}

