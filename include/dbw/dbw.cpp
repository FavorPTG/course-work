#include "dbw.h"
#include <iostream>
//#include "../gui/ahmgui.h"
#include "../CLI/clh.h"


int createPlaneTable(sqlite3 *db){
    const string createTableQuery =
            "CREATE TABLE IF NOT EXISTS planes ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "standend TEXT NOT NULL,"
            "stime TEXT NOT NULL,"
            "etime TEXT NOT NULL,"
            "rnumb INTEGER NOT NULL,"
            "nseats INTEGER NOT NULL,"
            "seatprice INTEGER NOT NULL"
            ");";
    int rc = sqlite3_exec(db, createTableQuery.c_str(), nullptr, nullptr, nullptr);
    if(rc != SQLITE_OK){
        cout << "Error from open database - tickets" << endl;
        return rc;
    }
    return SQLITE_OK;
}

int createTicketTable(sqlite3 *db){
    const string createBaseTableQery =
        "CREATE TABLE IF NOT EXISTS ticket ("
        "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "rnumb INTEGER NOT NULL,"
        "seatnumber INTEGER NOT NULL,"
        "name TEXT NUT NULL);";
    int rc = sqlite3_exec(db, createBaseTableQery.c_str(), nullptr, nullptr, nullptr);
    if(rc != SQLITE_OK){
        cout << "Error from creation table - planes" << endl;
        exit(rc);
        return rc;
    }
    return SQLITE_OK;
}

int deleteTable(sqlite3 *db, string tableName){
    const string deleteTableQuery = "DROP TABLE IF EXISTS " + tableName;
    int rc = sqlite3_exec(db, deleteTableQuery.c_str(), nullptr, nullptr, nullptr);
    return rc;
}

int deleteFromTable(sqlite3 *db, string tableName, string colName, string data){
	string delete_query = "DELETE FROM " + tableName + " WHERE " + colName + " = " + data + ";";

	char* errMsg;
	int rc = sqlite3_exec(db, delete_query.c_str(), nullptr, nullptr, &errMsg);

	if (rc != SQLITE_OK) {
		cerr << "SQL error: " << errMsg << endl;
        	sqlite3_free(errMsg);
    	} else
        	cout << "Record with " << colName << " " << data << " deleted successfully." << endl;

	return rc;
}

int deleteFromTable(sqlite3 *db, string tableName, string id){
	return deleteFromTable(db, tableName, "id", id);
}

int getDataFromTable(sqlite3 *db, string tableName){
    const string selectQuery = "SELECT * FROM " + tableName + ";";

    auto callback = [](void *data, int argc, char **argv, char **azColName) -> int {
        for (int i = 0; i < argc; ++i) {
            std::cout << azColName[i] << " = " << argv[i] << ";  ";
        }
 	cout << endl;
        return 0;
    };

    int rc = sqlite3_exec(db, selectQuery.c_str(), callback, nullptr, nullptr);
    if (rc != SQLITE_OK) {
        cout << "Error from open database" << endl;
        return rc;
    }

    return SQLITE_OK;
}

int findInTable(sqlite3 *db, string tableName, string colName, string fData){
	sqlite3_stmt* stmt;
	const string sql = "SELECT COUNT(" + colName + ") FROM " + tableName + " WHERE " + colName + " = " + fData + ";";
	int result = 0;
	if (sqlite3_prepare(db, sql.c_str(), -1, &stmt, nullptr) == SQLITE_OK) {
		/* cout << "ALL GOOD!!!" << endl; */
        	if (sqlite3_step(stmt) == SQLITE_ROW) {
                	result = sqlite3_column_int(stmt, 0); // Получаем значение COUNT(*)
                	/* std::cout << "Number of rows in 'ticket' table: " << result << std::endl; */
            	}
            	sqlite3_finalize(stmt);
        }
	
	return result;
}

int multyFindInTable(sqlite3 *db, string tableName, string col1, string f1, string col2, string f2){
	sqlite3_stmt *stmt;
	const string query = "SELECT COUNT(*) FROM " + tableName + " WHERE " + col1 + " = '" + f1 + "' AND " + col2 + " = '" + f2 + "';";
	int result = 0;
	if(sqlite3_prepare(db, query.c_str(), -1, &stmt, nullptr) == SQLITE_OK){
		/* cout << "OK" << endl; */
		if(sqlite3_step(stmt) == SQLITE_ROW){
			result = sqlite3_column_int(stmt, 0);
		}
		sqlite3_finalize(stmt);
	}
	return result;
}

int getDataById(sqlite3 *db, string tableName, string col, int id){
	sqlite3_stmt* stmt;
    	int age;
	int rc;

    	// Подготавливаем SQL запрос
    	const string sql = "SELECT " + col + " FROM " + tableName + " WHERE id = ?;";
	rc = sqlite3_prepare(db, sql.c_str(), -1, &stmt, nullptr);

	if (rc != SQLITE_OK) {
		std::cerr << "Ошибка подготовки запроса: " << sqlite3_errmsg(db) << std::endl;
		return 1;
	}

	// Привязываем значение ID к параметру запроса
	sqlite3_bind_int(stmt, 1, id);

	// Выполняем запрос
    	rc = sqlite3_step(stmt);

    	if (rc == SQLITE_ROW) {
        	// Извлекаем значение из столбца age 
		age = sqlite3_column_int(stmt, 0);
	}

	// Финализируем запрос
	sqlite3_finalize(stmt);

	return age;
}


int getDataWithDesc_back(sqlite3 *db, string query, string desc){
	
	sqlite3_stmt *stmt;
	int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);
	if (rc != SQLITE_OK) {
		cout << "error: " << sqlite3_errmsg(db) << endl;
    		return SQLITE_ERROR;
	}
	cout << desc << ":" << endl << "| ";
	while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
		const unsigned char *name = sqlite3_column_text(stmt, 0);
		cout << name << " | ";
	}
	cout << endl;
	
	if (rc != SQLITE_DONE) {
		cout << "error: " << sqlite3_errmsg(db) << endl;
	}
	
	sqlite3_finalize(stmt);

	return SQLITE_OK;
}

int getDataWithDesc(sqlite3 *db, string tableName, string colName, string desc){
	const string query = "SELECT " + colName + " FROM " + tableName + ";";
	return getDataWithDesc_back(db, query, desc);
}

int getDataWithDesc(sqlite3 *db, string tableName, string colO, string colS, string fil, string desc){
	const string query = "SELECT " + colO + " FROM " + tableName + " WHERE " + colS + " = " + fil + ";";
	return getDataWithDesc_back(db, query, desc);
}

int addNewPlaneInTable(sqlite3 *db){

    string startAndEnd, sTime, eTime;
    int rNumber, nSeats, seatPrice;
    cout << "Enter your point of departure and arrival: ";
    cin >> startAndEnd;
    cout << "Enter time start of flyght: ";
    cin >> sTime;
    cout << "Enter time end of flyght: ";
    cin >> eTime;
    cout << "Enter flight number: ";
    cin >> rNumber;
    cout << "Enter number of seats: ";
    cin >> nSeats;
    cout << "Enter the price on seat: ";
    cin >> seatPrice;

    const string instrQtuery = "INSERT INTO planes (standend, stime, etime, rnumb, nseats, seatprice) VALUES ('" +
            startAndEnd + "', '" + sTime + "', '" + eTime + "', " + to_string(rNumber) + ", " + to_string(nSeats) + ", " + to_string(seatPrice) + ")";

    int 
	rc = SQLITE_ERROR,
	q = findInTable(db, "planes", "rnumb", to_string(rNumber));

    if(q == 0)
	    rc = sqlite3_exec(db, instrQtuery.c_str(), nullptr, nullptr, nullptr);

    if(rc != SQLITE_OK)
        cout << "Error from adding the new plane" << endl;
    else 
	cout << "New flight added in the table" << endl;

    return rc;
}

int addNewTicketInTable(sqlite3 *db){
    string name;
    int rNumber, seatNumber;
    // Ввод номера рейса
    getDataWithDesc(db, "planes", "rnumb", "Номера доступных рейсов");
    cout << "Enter flight number: ";
    cin >> rNumber;
    cout << "----------" << endl;
    // Ввод номера места
    getDataWithDesc(db, "ticket", "seatnumber", "rnumb", to_string(rNumber), "Занятые места на рейсе "+to_string(rNumber));
    cout << "Enter seat number: ";
    cin >> seatNumber;
    cout << "----------" << endl;
    // Ввод имени владельца билета
    cout << "Enter name of owner: ";
    cin >> name;

    const string instrtQuery = "INSERT INTO ticket (rnumb, seatnumber, name) VALUES (" +
        to_string(rNumber) + ", " + to_string(seatNumber) + ", '" + name + "');";

    int
	rc = SQLITE_ERROR,
	q1 = findInTable(db, "planes", "rnumb", to_string(rNumber)),
	q2 = multyFindInTable(db, "ticket", "rnumb", to_string(rNumber), "seatnumber", to_string(seatNumber)),
	seatC = 0;


    if((q1 != 0) && (q2 == 0)){ 
	    for(int i=1; 1; i++){
		    if(getDataById(db, "planes", "rnumb", i) == rNumber){
			    seatC = getDataById(db, "planes", "nseats", i);
			    break;
		    }
	    }
	    if(seatNumber <= seatC)
		    rc = sqlite3_exec(db, instrtQuery.c_str(), nullptr, nullptr, nullptr);
    }

    if(rc != SQLITE_OK)
	cout << "Error from adding the new ticket" << endl;
    else
	cout << "Ticket added in table" << endl;

    return rc;
}

int joinTables(sqlite3 *db){
    createPlaneTable(db);
    createTicketTable(db);
    return 0;
}

void showMenu(){
    cout
        << "List of commands:" << endl
        << "1) Show all commands" << endl
        << "2) Add new plane" << endl
        << "3) Show all planes" << endl
        << "4) Buy the ticket" << endl
        << "5) Show all tickets" << endl
        << "6) Clear the tickets table" << endl
        << "7) Clear the planes table" << endl
        << "dp) Delete from planes table" << endl
	<< "dt) Delete from tickets table" << endl
        << "c) Clear the screen" << endl
        << "e) Exit" << endl;
        //<< "u) Update (if you delete the table)" << endl << endl;
}

int doCommand(sqlite3 *data){
    string cmd = "";
    cout << "> ";
    getline(cin, cmd);

    if((cmd == "1") || (cmd == "h") || (cmd == "H")){
        showMenu();
    }
    else if(cmd == "2"){
        addNewPlaneInTable(data);
    }
    else if(cmd == "3"){
        cout << "Data from 'planes' table:" << endl;
        getDataFromTable(data, "planes");
    }
    else if(cmd == "4"){
        addNewTicketInTable(data);
    }
    else if(cmd == "5"){
        cout << "Data from 'tickets' table:" << endl;
        getDataFromTable(data, "ticket");
    }
    else if((cmd == "6") || (cmd == "7")){
	deleteTable(data, (cmd == "6" ? "planes" : "ticket"));
	joinTables(data);
    }
    else if(cmd == "dp"){
	string n;
	getDataWithDesc(data, "planes", "rnumb", "Номера доступных рейсов");
	cout << "Enter flight number to delete: ";
	cin >> n;
	deleteFromTable(data, "planes", "rnumb", n);
	deleteFromTable(data, "ticket", "rnumb", n);
    }
    else if(cmd == "dt"){
	string n;
	getDataFromTable(data, "ticket");
	cout << "Enter ID of ticket to delete: ";
	cin >> n;
	deleteFromTable(data, "ticket", n);
    } 
    else if((cmd == "c") || (cmd == "C")){
        system("clear");
    } 
    else if((cmd == "e") || (cmd == "E") || (cmd == "q") || (cmd == "Q")){
        sqlite3_close(data);
	exit(0);
    }

    doCommand(data);
    return SQLITE_OK;
}
