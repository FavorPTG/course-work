#include "../sqlite/sqlite3.h"
#include "../nanogui.h"
#include <string>
#include <iostream>

using namespace std;

string to_str(const unsigned char*);

class DBWC {
	sqlite3 *db;
	std::string tables[2] = {"planes", "tickets"};

	int findInTable(string, string, string);
	int multyFindInTable(string, string, string, string, string);
	int getDataById(string, string, int);
	int getDataWithDesc_back(string, string);
	int getDataWithDesc(string, string, string);
	int getDataWithDesc(string, string, string, string, string);
	int deleteTable(string);
	int deleteFromTable(string, string, string);
	int deleteFromTable(string, string);
	int getTableSize(bool=true);

public:
	struct Plane {
		int id;
		string sAndE;
		string sTime;
		string eTime;
		int rNumber;
		int nSeats;
		int seatPrice;
	};

	struct Ticket {
		int id;
		int rNumber;
		int seatNumber;
		string name;
	};

	DBWC(sqlite3*);

	int createPlaneTable();
	int createTicketTable();
	int joinTables();

	Plane* getDataFromPlaneTable();
	Ticket* getDataFromTicketTable();

	int addNewPlaneInTable(Plane);
	int addNewTicketInTable(Ticket);	
};





/*
 * Function for listen the commands in custom shell
*/
int doCommand(sqlite3*);

/*
 * Backend function for create table of planes
*/
int createPlaneTable(sqlite3 *db);

/*
 * Backend function for create table of tickets
*/
int createTicketTable(sqlite3 *db);

/*
 * Function for delete the table by name
*/
int deleteTable(sqlite3 *db, string tableName);

/*
 * Delete record by column name and data in this column 
*/
int deleteFromTable(sqlite3 *db, string tableName, string colName, string data);

/*
 * Delete record by id
*/
int deleteFromTable(sqlite3 *db, string tableName, string id);

/*
 * Show the all data from table by name
*/
int getDataFromTable(sqlite3 *db, string tableName);

/*
 * Return the count of finded records
 *
 ** colName - name of column to finding
 ** fData - what we are looking for 
*/
int findInTable(sqlite3 *db, string tableName, string colName, string fData);

/*
 * Like findInTable, but allows you to find the number of records using two parametres
 ** col1 - name of column 1
 ** f1 - first parameter
 ** col2 - name of column 2
 ** f2 - second parameter
*/
int multyFindInTable(sqlite3 *db, string tableName, string col1, string f1, string col2, string f2);

/*
 * Returns data from column by id 
*/
int getDataById(sqlite3 *db, string tableName, string col, int id);

/*
 * Backend function for 'getDataWithDesc'
*/
int getDataWithDesc_back(sqlite3 *db, string query, string desc);

/*
 * Print the description and all records from column
*/
int getDataWithDesc(sqlite3 *db, string tableName, string colName, string desc);

/*
 * Print the description and all records from column 'colO' with filter 'colS=fil' 
*/
int getDataWithDesc(sqlite3 *db, string tableName, string colO, string colS, string fil, string desc);

/*
 * Adding the new record in 'planes' table 
*/
int addNewPlaneInTable(sqlite3 *db);

/*
 * Adding the new record in 'ticket' table
*/
int addNewTicketInTable(sqlite3 *db);

/*
 * Backend function for create 'planes' and 'ticket' tables 
*/
int joinTables(sqlite3 *db);

/*
 * Print menu 
*/
void showMenu();

/*
 * Listen the custom shell commands
*/
int doCommand(sqlite3 *data);

