#include "ahmgui.h"
#include "../nanogui.h"
#include "../nanogui/common.h"
#include "../nanogui/vector.h"
#include "../dbw/dbw.h"
//#include "../nanogui/opengl.h"
#include <GLFW/glfw3.h>
//#include <cstddef>
//#include <cstdlib>
#include <string>

#include <iostream>

using namespace nanogui;

typedef nanogui::ref<Window> rWindow;

struct Menus {
	rWindow pMenu;
};

rWindow create_add_plane_menu(FormHelper *gui){
	rWindow window = gui->add_window(Vector2i(10, 20), "Add plane");

	std::string startAndEnd = "Flight name";
	std::string sTime = "12:00";

	//auto bse = gui->add_group("Base");
	auto s1 = gui->add_variable("Point of departure and arrival", startAndEnd);
	s1->set_callback([s1](const std::string &s){ 
		//startAndEnd = s1->value();
		std::cout << s1->value() << std::endl;
	});

	auto s2 = gui->add_variable("Time start of flyght", sTime);
	s2->set_callback([s1](const std::string &s){
		//sTime = s;
		//std::cout << s << std::endl;
		std::cout << s1->value() << std::endl;
	});
	gui->add_button("Add", [s1](){ std::cout << s1->value() << std::endl; });
	/* auto b = gui->add_button("Add", nullptr); */
	/* b->set_callback([&startAndEnd](){ */
	/* 	std::cout << startAndEnd << std::endl; */
	/* }); */

	window->center();
	//window->set_visible(false);
	
	return window;
}

rWindow create_main_menu(FormHelper *gui, Menus menu){
	bool bvar = true;

	rWindow window = gui->add_window(Vector2i(10, 10), "AirHubManager menu");

	gui->add_group("Show menu");
	auto b = gui->add_variable("Add plane", bvar);
	b->set_callback([&menu](bool t){
		/* if(t){ */
		/* 	std::cout << "Its True!!!" << std::endl; */
		/* } */
		menu.pMenu->set_visible(t);
	});
	//auto ctx = gui->window()->screen()->nvg_context();
	/* gui->add_group("Validating fields"); */
	/*         gui->add_variable("int", ivar)->set_spinnable(true); */
	/* gui->add_variable("float", fvar); */
	/*         gui->add_variable("double", dvar)->set_spinnable(true); */

	gui->add_group("Menu");
	//gui->add_button("Add plane", [&menu, &ctx](){ std::cout << "Add palne" << std::endl; menu.pMenu->set_visible(!menu.pMenu->visible()); /*Menu.pMenu->draw(ctx);*/ });
	gui->add_button("Exit", []() { shutdown(); exit(0); });
	
	//window->center();
	//window->mouse_drag_event({0, 0}, {10, 10}, 0, 0);

	return window;
}

int init_gui(bool use_gl_4_1, bool fullscreen_mode)
{
	nanogui::init();

	{
	        Screen *screen = nullptr;
		Menus menu;

        	if (use_gl_4_1) {
            		screen = new Screen(Vector2i(700, 700), "AirHubManager GUI [GL 4.1]", true, fullscreen_mode, true, true, true, 4, 1);
        	} else {
            		screen = new Screen(Vector2i(700, 700), "AirHubManager GUI");
        	}

		FormHelper *gui = new FormHelper(screen);
		
		menu.pMenu = create_add_plane_menu(gui);
		auto mMenu = create_main_menu(gui, menu);
	
		//screen->dec_ref();
		screen->draw_all();
		screen->set_visible(true);
        	screen->perform_layout();

		nanogui::mainloop();
	}

	nanogui::shutdown();
	return 0;
}



class Application : public Screen {
public:	
	Application(sqlite3 *db) : Screen(Vector2i(700, 700), "AirHubManager GUI") {
		DBWC dbwc = DBWC(db);
		
		inc_ref();
		Window *mainWin, *planeWin, *showPlanesWin;

		// start init the planes widget
		planeWin = new Window(this, "Add plane");
		planeWin->center();
		GridLayout *layout = new GridLayout(Orientation::Horizontal, 2, 
				Alignment::Middle, 15, 5);
		layout->set_col_alignment({Alignment::Maximum, Alignment::Fill});
		layout->set_spacing(0, 10);
		planeWin->set_layout(layout);

		TextBox *tb[] = {
			addNamedText(planeWin, "Point of departure and arrival:", "Moscow-Kazan"),
			addNamedText(planeWin, "Start time:", "10:00"),
			addNamedText(planeWin, "End time:", "12:00"),
			addNamedText(planeWin, "Flight number", "1234", true),
			addNamedText(planeWin, "Seats count:", "100", true),
			addNamedText(planeWin, "Seat price:", "1500", true)
		};
		const int tbs = sizeof(tb) / sizeof(void*);	

		PopupButton *popup_btn = new PopupButton(planeWin, "        Current plane");
        	popup_btn->set_side(Popup::Side::Left);
        	Popup *popup_left = popup_btn->popup();
        	popup_left->set_layout(new GroupLayout());
        	
		addLabel(popup_left, "Plane:", true);
		Label *labs[tbs];
		
		for(int i=0; i<tbs; i++)
			labs[i] = addLabel(popup_left, tb[i]->value(), false);
		
		popup_btn->set_callback([labs, tb](){
			for(int i=0; i<6; i++)
				labs[i]->set_caption(tb[i]->value());
		});
		
		Button *b = new Button(planeWin, "Add");
		b->set_callback([db, tb, &dbwc](){ 
			DBWC::Plane p = { 
				0, 
				tb[0]->value(), 
				tb[1]->value(), 
				tb[2]->value(), 
				stoi(string(tb[3]->value())), 
				stoi(string(tb[4]->value())), 
				stoi(string(tb[5]->value())) 
			};

			DBWC::Plane p1 = {0, "E-G", "1", "2", 1234, 1, 2};
			dbwc.addNewPlaneInTable(p);
			//dbwc.addNewPlaneInTable(p);
			//dbwc.findInTable("planes", "rnumb", to_string(p.rNumber));
			//std::cout << "Plane is added" << std::endl; 
		});
		// end init the plane widget
		

		// start init the show planes widget 
		showPlanesWin = new Window(this, "Show planes");
		showPlanesWin->set_position(Vector2i(200, 10));
		showPlanesWin->set_layout(layout);
		addLabel(showPlanesWin, " ");
		addLabel(showPlanesWin, "Show planes");

		DBWC::Plane p = dbwc.getDataFromPlaneTable();
		addLabel(showPlanesWin, "Название: "+p.sAndE, false);
		addLabel(showPlanesWin, "Время вылета: "+p.sTime, false);
		addLabel(showPlanesWin, "Время прибытия: "+p.eTime, false);
		addLabel(showPlanesWin, "Номер рейса: "+to_string(p.rNumber), false);
		addLabel(showPlanesWin, "Количество мест: "+to_string(p.nSeats), false);
		addLabel(showPlanesWin, "Стоимость одного места: "+to_string(p.seatPrice), false);
		// end init the show planes widget 
		

		// start init the main widget
		mainWin = new Window(this, "AirHubManager menu");
		mainWin->set_position(Vector2i(10, 10));
		mainWin->set_layout(new GroupLayout());

		addLabel(mainWin, "Show menu");
		CheckBox *cb = new CheckBox(mainWin, "Add palne");
		cb->set_checked(true);
		cb->set_callback([planeWin](bool b){ planeWin->set_visible(b); });

		cb = new CheckBox(mainWin, "Show planes");
		cb->set_checked(true);
		cb->set_callback([showPlanesWin](bool b){ showPlanesWin->set_visible(b); });

		//new Label(window, "Menu", "sans-bold");
		addLabel(mainWin, "Menu");
		b = new Button(mainWin, "Exit");
		b->set_fixed_height(25);
		b->set_callback([](){ shutdown(); exit(0); });
		// end init the main widget
		
		perform_layout();
	}

	Label* addLabel(Window *win, std::string text, bool isHeader=true){
		int fontSize = (isHeader ? 20 : 16);
		std::string fontStyle = (isHeader ? "sans-bold" : "sans");
		Label *l = new Label(win, text, fontStyle);
		l->set_font_size(fontSize);
		return l;	
	}

	TextBox* addNamedText(Window *win, std::string label, std::string def_value, bool isIntBox=false){
		new Label(win, label, "sans-bold");
		if(!isIntBox){
			TextBox *text_box = new TextBox(win);
            		text_box->set_editable(true);
            		text_box->set_fixed_size(Vector2i(130, 30));
            		text_box->set_value(def_value);
            		text_box->set_default_value(def_value);
            		text_box->set_font_size(15);
			return text_box;
		} else{
			auto int_box = new IntBox<int>(win);
            		int_box->set_editable(true);
            		int_box->set_fixed_size(Vector2i(130, 30));
			int_box->set_value(atoi(def_value.c_str()));
            		int_box->set_default_value(def_value);
            		int_box->set_font_size(17);
            		int_box->set_format("[1-9][0-9]*");
           	 	int_box->set_spinnable(true);
        		int_box->set_min_value(1);
            		int_box->set_value_increment(1);
			return int_box;
		}
	}


	virtual bool keyboard_event(int key, int scancode, int action, int modifiers) {
        	if (Screen::keyboard_event(key, scancode, action, modifiers))
        		return true;
        	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
            		set_visible(false);
            		return true;
        	}
        	return false;
    	}	
};


void init_gui_v2(){
	nanogui::init();

	{
		sqlite3 *base;
		int rc = sqlite3_open("database.db", &base);
		if(rc != SQLITE_OK){
			cout << "Error from open database" << " Error code " << rc << endl;
			exit(rc);
    		}

		nanogui::ref<Application> app = new Application(base);
		app->dec_ref();
		app->draw_all();
		app->set_visible(true);
		nanogui::mainloop(1 / 60.f * 1000);
	}

	nanogui::shutdown();
}
