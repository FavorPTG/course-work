#include <iomanip>
#include <iostream>
#include <string>
#include "include/dbw/dbw.h"
#include "include/CLI/clh.h"
#include "include/gui/ahmgui.h"

using namespace std;


/*
** пункт учёта в аэропорту
**
** Доступные рейсы
** Для доступного рейса свободные места
** Возможноть покупки места (у места есть поля: номер, стоимость, имя владельца)
**
** Таблица описывает место в самолёте: id (int), пункты отправления-прибытия (string),
** время отправления (string), время прибытия (string), номер места (int), стоимость
** места (int), имя владельца (string)

** TODO: добавить возможность изменения времени полёта
*/


int main(int argc, char** argv){
    	setlocale(LC_ALL, "Russian");
    	sqlite3 *data;
    	int rc = sqlite3_open("database.db", &data);
    	if(rc != SQLITE_OK){
    		cout << "Error from open database" << endl;
        	return rc;
    	}

	/* DBWC *dbwc = new DBWC(data); */
	/* cout << "#####################" << endl; */
	if(argc == 1) {
		/* Menu *menu = new Menu; */
		/* DBWC *dbw = new DBWC(data); */
		/* DBWC::Plane p; */
		/* menu->addItem("Add new plane", [&p, &dbw](int i){ */
		/* 	cout << "Adding new plane" << endl; */
		/* 	cout << "Enter flight number: "; */
		/* 	cin >> p.rNumber; */
		/* 	cout << "Enter flight name: "; */
		/* 	cin >> p.rNumber; */
		/* 	cout << "Enter start time: "; */
		/* 	cin >> p.sTime; */
		/* 	cout << "Enter end time: "; */
		/* 	cin >> p.eTime; */
		/* 	cout << "Enter count of seats: "; */
		/* 	cin >> p.nSeats; */
		/* 	cout << "Enter price of one seat: "; */
		/* 	cin >> p.seatPrice; */
		/* }); */
		/* 	    	dbw->addNewPlaneInTable({0, "Zal-Kuhnya", "10:00", "10:02", 12, 1, 0}); */
		/* DBWC::Plane *plns = dbw->getDataFromPlaneTable(); */
		/* 	    	for(int i{0}; i<5; i++){ */
		/* 	cout << "Plane №" << plns[i].rNumber << endl  */
		/* 		<< "	" << plns[i].sAndE << endl */
		/* 		<< "	" << plns[i].sTime << endl */
		/* 		<< "	" << plns[i].eTime << endl */
		/* 		<< "	" << plns[i].nSeats << endl */
		/* 		<< "	" << plns[i].seatPrice << endl; */
		/* 	cout << "#####################" << endl; */
		/* } */

	    //cout << dbw->getDataFromTable("planes") << endl;
	    //const unsigned char *c;
	    //cout << to_str(c) << endl;

		joinTables(data);
		showMenu();
		doCommand(data);
	} 
	else {
		for(int i=0; i<argc; i++){ 
			string s = argv[i];
			if(s == "-gui1")
				init_gui(false, false);
			else if(s == "-gui2")
				init_gui_v2();
	    }
	}

    	return 0;
}

