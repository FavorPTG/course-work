.PHONY: all clean install uninstall

TARGET = dbt
PFX = /usr/local/bin
SRC = main.cpp include/sqlite/sqlite3.c include/dbw/dbw.cpp include/gui/ahmgui.cpp include/CLI/clh.h
OBJ = obj/main.o obj/sqlite3.o obj/dbw.o obj/ahmgui.o obj/clh.o obj/dbc.o

all: ${TARGET}
	
clean:
	rm -rf ${TARGET} obj/*.o obj 

obj/main.o: main.cpp
	g++ -c -o obj/main.o main.cpp

obj/sqlite3.o: include/sqlite/sqlite3.c
	g++ -c -o obj/sqlite3.o include/sqlite/sqlite3.c

obj/dbw.o: include/dbw/dbw.cpp 
	g++ -c -o obj/dbw.o include/dbw/dbw.cpp 

obj/dbc.o: include/dbw/dbc.cpp 
	g++ -c -o obj/dbc.o include/dbw/dbc.cpp 

obj/ahmgui.o: include/gui/ahmgui.cpp 
	g++ -c -o obj/ahmgui.o include/gui/ahmgui.cpp 

obj/clh.o: include/CLI/clh.cpp 
	g++ -c -o obj/clh.o include/CLI/clh.cpp

${TARGET}: ${OBJ}
	g++ -o ${TARGET} ${OBJ} -L./lib -lnanogui

install:
	install ${TARGET} ${PFX}

uninstall:
	rm -rf ${PFX}/${TARGET}
